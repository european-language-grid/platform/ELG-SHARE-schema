<?xml version="1.0" encoding="UTF-8"?>
<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://w3id.org/meta-share/meta-share/" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://live.european-language-grid.eu/metadata-schema/ELG-SHARE.xsd">
	<ms:DescribedEntity>
		<ms:LanguageResource>
			<ms:entityType>LanguageResource</ms:entityType>
			<ms:resourceName xml:lang="en">bBootstrapped Lexicon of English Verbal Polarity Shifters</ms:resourceName>
			<ms:description xml:lang="en">We provide a bootstrapped lexicon of English verbal polarity shifters. Our lexicon covers 3043 verbs of WordNet v3.1 (Miller et al., 1990) that are single word or particle verbs. Polarity shifter labels are given for each word lemma.
Data
The data consists of:
Two lists of WordNet verbs (Miller et al., 1990), annotated for whether they cause shifting.
The initial gold standard (§2) of 2000 randomly chosen verbs.
The bootstrapped 1043 verbs (§5.3) that were labelled as shifters by our best classifier and then manually annotated.
Data set of verb phrases from the Amazon Product Review Data corpus (Jindal &amp; Liu, 2008), annotated for polarity of phrase and polar noun.
 
1. Verbal Shifters
Files
The initial gold standard: verbal_shifters.gold_standard.txt
The bootstrapped verbs: verbal_shifters.bootstrapping.txt
Format
Each line contains a verb and its label, separate by a whitespace.
Multiword expressions are separated by an underscore (WORD_WORD).
All labels were assigned by an expert annotator.
 
2. Sentiment Verb Phrases
Files
All annotated verb phrases: sentiment_phrases.txt
Content
The file starts with 400 phrases containing shifter verbs, followed by 2231 phrases containing non-shifter verbs.
Format
Every item consists of:
The sentence from which the VP and the polar noun were extracted.
The VP, polar noun and the verb heading the VP.
Constituency parse for the VP.
Gold labels for VP and polar noun by a human annotator.
Predicted labels for VP and polar noun by RNTN tagger (Socher et al., 2013) and LEX_gold approach.
Items are separated by a line of asterisks (*)
Related Resources
Paper: ACL Anthology or DOI: 10.5281/zenodo.3365609
Presentation: ACL Anthology
Word Embedding: DOI: 10.5281/zenodo.3370051
Attribution
This dataset was created as part of the following publication:
Marc Schulder, Michael Wiegand, Josef Ruppenhofer and Benjamin Roth (2017). "Towards Bootstrapping a Polarity Shifter Lexicon using Linguistic Features". Proceedings of the 8th International Joint Conference on Natural Language Processing (IJCNLP). Taipei, Taiwan, November 27 - December 3, 2017. DOI: 10.5281/zenodo.3365609.
If you use the data in your research or work, please cite the publication.
 
This work was partially supported by the German Research Foundation (DFG) under grants RU 1873/2-1 and WI4204/2-1.</ms:description>
			<ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/doi">http://doi.org/10.5281/zenodo.3364812b </ms:LRIdentifier>
			<ms:version>1.0.0</ms:version>
			<ms:additionalInfo>
				<ms:landingPage>https://zenodo.org/record/3364812#.XvNJNWgzaMo</ms:landingPage>
			</ms:additionalInfo>
			<ms:keyword xml:lang="en">Sentiment Analysis</ms:keyword>
			<ms:keyword xml:lang="en">Sentiment Polarity</ms:keyword>
			<ms:keyword xml:lang="en">Negation</ms:keyword>
			<ms:keyword xml:lang="en">Lexical Semantics</ms:keyword>
			<ms:keyword xml:lang="en">Lexicon</ms:keyword>
			<ms:keyword xml:lang="en">NLP Resources</ms:keyword>
			<ms:LRSubclass>
				<ms:LexicalConceptualResource>
					<ms:lrType>LexicalConceptualResource</ms:lrType>
					<ms:lcrSubclass>http://w3id.org/meta-share/meta-share/computationalLexicon</ms:lcrSubclass>
					<ms:encodingLevel>http://w3id.org/meta-share/meta-share/semantics</ms:encodingLevel>
					<ms:ContentType>http://w3id.org/meta-share/meta-share/SemanticType</ms:ContentType>
					<ms:LexicalConceptualResourceMediaPart>
						<ms:LexicalConceptualResourceTextPart>
							<ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
							<ms:mediaType></ms:mediaType>
							<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
							<ms:language>
								<ms:languageTag>en</ms:languageTag>
								<ms:languageId>en</ms:languageId>
							</ms:language>
						</ms:LexicalConceptualResourceTextPart>
					</ms:LexicalConceptualResourceMediaPart>
					<ms:DatasetDistribution>
						<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
						<ms:accessLocation>https://zenodo.org/record/3364812#.XvNJNWgzaMo</ms:accessLocation>
						<ms:distributionTextFeature>
							<ms:size>
								<ms:amount>0</ms:amount>
								<ms:sizeUnit><ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:sizeUnitRecommended></ms:sizeUnit>
							</ms:size>
							<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:dataFormatRecommended></ms:dataFormat>
						</ms:distributionTextFeature>
						<ms:licenceTerms>
							<ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0 International</ms:licenceTermsName>
							<ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
							<ms:LicenceIdentifier ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX">CC-BY-4.0</ms:LicenceIdentifier>
						</ms:licenceTerms>
					</ms:DatasetDistribution>
					<ms:personalDataIncluded>http://w3id.org/meta-share/meta-share/noP</ms:personalDataIncluded>
					<ms:sensitiveDataIncluded>http://w3id.org/meta-share/meta-share/noS</ms:sensitiveDataIncluded>
				</ms:LexicalConceptualResource>
			</ms:LRSubclass>
		</ms:LanguageResource>
	</ms:DescribedEntity>
</ms:MetadataRecord> 
