from flask import Flask
from flask_restful import Api

from views import ValidateXML

app = Flask(__name__)
api = Api(app)
api.add_resource(ValidateXML, "/validate-xml/")

if __name__ == "__main__":
    app.run(port=8000, debug=True)
