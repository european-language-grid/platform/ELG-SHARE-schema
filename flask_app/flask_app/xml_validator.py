from urllib.parse import urlparse

import requests
from lxml import etree

from settings import XSD_PATH


class HTTPSResolver(etree.Resolver):
    __name__ = 'HTTPSResolver'

    def resolve(self, url, id_, context):
        url_components = urlparse(url)
        scheme = url_components.scheme
        # the resolver will only check for redirects if http/s is present
        if scheme == 'http' or scheme == 'https':
            head_response = requests.head(url, allow_redirects=True)
            new_request_url = head_response.url
            if len(head_response.history) != 0:
                # recursively, resolve the ultimate redirection target
                return self.resolve(new_request_url, id, context)
            else:
                if scheme == 'http':
                    # libxml2 can handle this resource
                    return self.resolve_filename(new_request_url, context)
                elif scheme == 'https':
                    # libxml2 cannot resolve this resource, so do the work
                    get_response = requests.get(new_request_url)
                    return self.resolve_string(get_response.content, context,
                                               base_url=new_request_url)
                else:
                    raise Exception("[%s] Something odd has happened - scheme should be http or https" %
                                    __name__)
        else:
            # treat resource as a plain old file
            return self.resolve_filename(url, context)


class Validator:
    def __init__(self, xsd_path: str):
        parser = etree.XMLParser(load_dtd=True)
        resolver = HTTPSResolver(xsd_path)
        parser.resolvers.add(resolver)
        self.xmlschema = etree.XMLSchema(etree.parse(xsd_path, parser=parser))

    def validate(self, xml_path: str) -> bool:
        xml_doc = etree.parse(xml_path)
        result = self.xmlschema.validate(xml_doc)

        return result

    def _parse_error(self, error):
        return {
            "line": error.line,
            "error": error.message.replace("{http://w3id.org/meta-share/meta-share/}", "")
        }

    def parse_errors(self, log, xml_file=None):
        errors = list()
        for error in log:
            errors.append(self._parse_error(error))
        return errors


VALIDATOR = Validator(XSD_PATH)
