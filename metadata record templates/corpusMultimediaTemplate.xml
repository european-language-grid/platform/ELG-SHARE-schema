<?xml version="1.0" encoding="UTF-8"?>
<!--Pre-filled metadata record as an example for corpora
Use case: a multimedia corpus composed of video recordings of lectures in English, the audio recordings, their transcriptions, the subtitles in English and their translations into German 
More information: https://european-language-grid.readthedocs.io/en/release1.0.0/all/RegisterCorpus.html -->
<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://w3id.org/meta-share/meta-share/" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://live.european-language-grid.eu/metadata-schema/ELG-SHARE.xsd">
	<ms:DescribedEntity>
		<ms:LanguageResource>
			<ms:entityType>LanguageResource</ms:entityType>
			<!-- please try to use "unique" names for the resource, rather than a general title such as "corpus of articles from legal journals" -->
			<ms:resourceName xml:lang="en">full title of a corpus</ms:resourceName>
			<ms:resourceShortName xml:lang="en">short name</ms:resourceShortName>
			<ms:description xml:lang="en">Short description of the resource for human readers with the basic features (e.g., language, size, format, domain or genre, etc); the example for this template is for an video of lectures in English, the audio recordings, their transcriptions, the subtitles in English and their translations into German</ms:description>
			<!-- the LRIdentifier is a multiple element; if you already have an identifier (e.g. handle PID, ISLRN, DOI, please add with the appropriate LRIdentifierScheme value -->
			<ms:LRIdentifier ms:LRIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned</ms:LRIdentifier>
			<!-- if you have a version number, replace; otherwise, leave as is -->
			<ms:version>1.0.0 (automatically assigned)</ms:version>
			<!-- additionalInfo can be either a general email address or a landing page (a web page with general information, including contact details; if you want, you can also provide a contact person details) -->
			<ms:additionalInfo>
				<ms:email>info@example.com</ms:email>
				<!--ms:landingPage>http://www.example.com</ms:landingPage> -->
			</ms:additionalInfo>
			<!-- RECOMMENDED element; use it only if you want, but PROVIDE an email for the person in this case -->
			<ms:contact>
				<ms:Person>
					<ms:actorType>Person</ms:actorType>
					<ms:surname xml:lang="en">Smith</ms:surname>
					<ms:givenName xml:lang="en">John</ms:givenName>
					<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
					<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
					<ms:email>smith@example.com</ms:email>
				</ms:Person>
			</ms:contact>
			<!-- mutliple element; please repeat for different keywords-->
			<ms:keyword xml:lang="en">multimedia corpus</ms:keyword>
			<ms:keyword xml:lang="en">video lectures</ms:keyword>
			<ms:keyword xml:lang="en">subtitles</ms:keyword>
			<!-- details for the organization that adds this resource in ELG -->
			<ms:resourceProvider>
				<ms:Organization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">Full name of the organization</ms:organizationName>
					<ms:website>http://www.example.com</ms:website>
				</ms:Organization>
			</ms:resourceProvider>
			<!-- RECOMMENDED element for citation purposes; the date the resource was first made available to the public and not just in ELG -->
			<ms:publicationDate>2020-01-09</ms:publicationDate>
			<!-- RECOMMENDED element; it can be one or more persons (repeat the element) or an organization  -->
			<ms:resourceCreator>
				<ms:Person>
					<ms:actorType>Person</ms:actorType>
					<ms:surname xml:lang="en">Smith</ms:surname>
					<ms:givenName xml:lang="en">John</ms:givenName>
					<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
					<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
					<ms:email>smith@example.com</ms:email>
				</ms:Person>
			</ms:resourceCreator>
			<!-- recommended: you can use one of the values from the LT taxonomy (recommended practice) or a free text value (you can also add the URL link to a class from another ontology) -->
			<ms:intendedApplication>
				<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
			</ms:intendedApplication>
			<ms:intendedApplication>
				<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/AutomaticSubtitling</ms:LTClassRecommended>
			</ms:intendedApplication>
			<ms:intendedApplication>
				<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/SpeechRecognition</ms:LTClassRecommended>
			</ms:intendedApplication>
			<!-- a link to a document, e.g. a user manual, a research article describing the resource, etc.-->
			<ms:isDocumentedBy>
				<ms:title xml:lang="en">free text for a document, e.g. title, full bibliographic record</ms:title>
				<!-- recommended: please provide a DOI or URL link to the document; if it's a DOI, please use the format of the example below -->
				<ms:DocumentIdentifier ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/doi">https://doi.org/10.1371/journal.pone.0077056</ms:DocumentIdentifier>
				</ms:isDocumentedBy>
			<ms:LRSubclass>
				<ms:Corpus>
					<ms:lrType>Corpus</ms:lrType>
					<!-- please use one of the values for raw corpus, annotated corpus (raw corpus together with annotations) or annotations (without the raw corpus) -->
					<ms:corpusSubclass>http://w3id.org/meta-share/meta-share/rawCorpus</ms:corpusSubclass>
					<!-- this part is used for the description of the subtitles and their translation -->
					<ms:CorpusMediaPart>
						<ms:CorpusTextPart>
							<ms:corpusMediaType>CorpusTextPart</ms:corpusMediaType>
							<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
							<ms:lingualityType>http://w3id.org/meta-share/meta-share/bilingual</ms:lingualityType>
							<ms:multilingualityType>http://w3id.org/meta-share/meta-share/parallel</ms:multilingualityType>
							<!-- languageTag is the maximal code according to BCP47, combining language and (optional) script, region and variant -->
							<ms:language>
								<ms:languageTag>en-US</ms:languageTag>
								<ms:languageId>en</ms:languageId>
								<ms:regionId>US</ms:regionId>
							</ms:language>
							<ms:language>
								<ms:languageTag>de</ms:languageTag>
								<ms:languageId>de</ms:languageId>
							</ms:language>
							<!-- recommended: if applicable, please use a free text for the text type, e.g. news, article, fiction, etc. -->
							<ms:textType>
								<ms:categoryLabel xml:lang="en">subtitle</ms:categoryLabel>
							</ms:textType>
						</ms:CorpusTextPart>
					</ms:CorpusMediaPart>
					<!-- this part is used for the description of the transcripts -->
					<ms:CorpusMediaPart>
						<ms:CorpusTextPart>
							<ms:corpusMediaType>CorpusTextPart</ms:corpusMediaType>
							<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
							<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
							<!-- languageTag is the maximal code according to BCP47, combining language and (optional) script, region and variant -->
							<ms:language>
								<ms:languageTag>en-US</ms:languageTag>
								<ms:languageId>en</ms:languageId>
								<ms:regionId>US</ms:regionId>
							</ms:language>
							<!-- recommended: if applicable, please use a free text for the text type, e.g. news, article, fiction, etc. -->
							<ms:textType>
								<ms:categoryLabel xml:lang="en">transcript</ms:categoryLabel>
							</ms:textType>
						</ms:CorpusTextPart>
					</ms:CorpusMediaPart>
					<ms:CorpusMediaPart>
						<!-- this part is used for the description of the audio recordings that have been extracted from the video -->
						<ms:CorpusAudioPart>
							<ms:corpusMediaType>CorpusAudioPart</ms:corpusMediaType>
							<ms:mediaType>http://w3id.org/meta-share/meta-share/audio</ms:mediaType>
							<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
							<ms:language>
								<ms:languageTag>en-US</ms:languageTag>
								<ms:languageId>en</ms:languageId>
								<ms:regionId>US</ms:regionId>
							</ms:language>
							<ms:AudioGenre>
								<ms:categoryLabel xml:lang="en">speech</ms:categoryLabel>
							</ms:AudioGenre>
							<ms:SpeechGenre>
								<ms:categoryLabel xml:lang="en">lecture</ms:categoryLabel>
							</ms:SpeechGenre>
							<ms:numberOfParticipants>2</ms:numberOfParticipants>
							<ms:geographicDistributionOfParticipants xml:lang="en">North Carolina</ms:geographicDistributionOfParticipants>
						</ms:CorpusAudioPart>
					</ms:CorpusMediaPart>
					<!-- this part is used for the description of the video recordings of the lectures -->
					<ms:CorpusMediaPart>
						<ms:CorpusVideoPart>
							<ms:corpusMediaType>CorpusVideoPart</ms:corpusMediaType>
							<ms:mediaType>http://w3id.org/meta-share/meta-share/video</ms:mediaType>
							<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
							<ms:language>
								<ms:languageTag>en-US</ms:languageTag>
								<ms:languageId>en</ms:languageId>
								<ms:regionId>US</ms:regionId>
							</ms:language>
							<ms:VideoGenre>
								<ms:categoryLabel xml:lang="en">videolecture</ms:categoryLabel>
							</ms:VideoGenre>
							<ms:typeOfVideoContent xml:lang="en">human beings</ms:typeOfVideoContent>
							<ms:typeOfVideoContent xml:lang="en">presentations</ms:typeOfVideoContent>
							<ms:numberOfParticipants>2</ms:numberOfParticipants>
							<ms:geographicDistributionOfParticipants xml:lang="en">North Carolina</ms:geographicDistributionOfParticipants>
						</ms:CorpusVideoPart>
					</ms:CorpusMediaPart>
					<ms:DatasetDistribution>
						<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
						<!-- one of the following is mandatory; if the corpus can be downloaded without any intermediate pages or clicks, please use downloadLocation; if accessible from an informative page that contains the download location, please use accessLocation -->
						<ms:downloadLocation>ftp://www.download.com</ms:downloadLocation>
						<ms:accessLocation>http://www.access.com</ms:accessLocation>
						<!-- recommended -->
						<ms:samplesLocation>http://samples.com</ms:samplesLocation>
						<ms:distributionTextFeature>
							<ms:size>
								<ms:amount>2</ms:amount>
								<ms:sizeUnit><ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/file</ms:sizeUnitRecommended></ms:sizeUnit>
							</ms:size>
							<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormatRecommended></ms:dataFormat>
							<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
						</ms:distributionTextFeature>
						<ms:distributionTextFeature>
							<ms:size>
								<ms:amount>230</ms:amount>
								<ms:sizeUnit><ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/sentence1</ms:sizeUnitRecommended></ms:sizeUnit>
							</ms:size>
							<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormatRecommended></ms:dataFormat>
							<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
						</ms:distributionTextFeature>
						<ms:distributionAudioFeature>
							<ms:size>
								<ms:amount>1</ms:amount>
								<ms:sizeUnit><ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/file</ms:sizeUnitRecommended></ms:sizeUnit>
							</ms:size>
							<ms:durationOfAudio>
								<ms:amount>3</ms:amount>
								<ms:durationUnit>http://w3id.org/meta-share/meta-share/hour</ms:durationUnit>
							</ms:durationOfAudio>
							<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/wav</ms:dataFormatRecommended></ms:dataFormat>
							<ms:audioFormat>
								<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/wav</ms:dataFormatRecommended></ms:dataFormat>
								<ms:compressed>true</ms:compressed>
							</ms:audioFormat>
						</ms:distributionAudioFeature>
						<ms:distributionVideoFeature>
							<ms:size>
								<ms:amount>3</ms:amount>
								<ms:sizeUnit><ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/file</ms:sizeUnitRecommended></ms:sizeUnit>
							</ms:size>
							<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/wav</ms:dataFormatRecommended></ms:dataFormat>
							<ms:videoFormat>
								<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/wav</ms:dataFormatRecommended></ms:dataFormat>
								<ms:compressed>true</ms:compressed>
							</ms:videoFormat>
						</ms:distributionVideoFeature>
						<ms:licenceTerms>
							<ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0 International</ms:licenceTermsName>
							<ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
							<ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
							<ms:LicenceIdentifier ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX">CC-BY-4.0</ms:LicenceIdentifier>
						</ms:licenceTerms>
					</ms:DatasetDistribution>
					<ms:personalDataIncluded>http://w3id.org/meta-share/meta-share/yesP</ms:personalDataIncluded>
					<ms:personalDataDetails xml:lang="en">videos tagged with names of the participants</ms:personalDataDetails>
					<ms:sensitiveDataIncluded>http://w3id.org/meta-share/meta-share/yesS</ms:sensitiveDataIncluded>
					<ms:sensitiveDataDetails xml:lang="en">videos with real persons showing their faces; consent forms have been signed</ms:sensitiveDataDetails>
					<ms:anonymized>http://w3id.org/meta-share/meta-share/noA</ms:anonymized>
				</ms:Corpus>
			</ms:LRSubclass>
		</ms:LanguageResource>
	</ms:DescribedEntity>
</ms:MetadataRecord>
