<?xml version="1.0" encoding="UTF-8"?>
<!--Pre-filled metadata record as an example for language descriptions
Use case: Machine Learning (ML) model
More information: https://european-language-grid.readthedocs.io/en/release1.0.0/all/RegisterLangDesc.html -->
<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://w3id.org/meta-share/meta-share/" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://live.european-language-grid.eu/metadata-schema/ELG-SHARE.xsd">
	<ms:DescribedEntity>
		<ms:LanguageResource>
			<ms:entityType>LanguageResource</ms:entityType>
			<!-- please try to use "unique" names for the resource, rather than a general title such as "5-gram model of English" -->
			<ms:resourceName xml:lang="en">full title of an n-gram model</ms:resourceName>
			<!-- RECOMMENDED -->
			<ms:resourceShortName xml:lang="en">short name</ms:resourceShortName>
			<ms:description xml:lang="en">Short description of the resource for human readers with the basic features (e.g., language, size, format, domain, etc); the example for this template is for a Machine Learning model of English</ms:description>
			<!-- the LRIdentifier is a multiple element; if you already have an identifier (e.g. handle PID, ISLRN, DOI, please add with the appropriate LRIdentifierScheme value -->
			<ms:LRIdentifier ms:LRIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned</ms:LRIdentifier>
			<!-- if you have a version number, replace; otherwise, leave as is -->
			<ms:version>1.0.0 (automatically assigned)</ms:version>
			<!-- additionalInfo can be either a general email address or a landing page (a web page with general information, including contact details; if you want, you can also provide a contact person details) -->
			<ms:additionalInfo>
				<ms:email>info@example.com</ms:email>
				<!--ms:landingPage>http://www.example.com</ms:landingPage> -->
			</ms:additionalInfo>
			<!-- RECOMMENDED element; use it only if you want, but PROVIDE an email for the person in this case -->
			<ms:contact>
				<ms:Person>
					<ms:actorType>Person</ms:actorType>
					<ms:surname xml:lang="en">Smith</ms:surname>
					<ms:givenName xml:lang="en">John</ms:givenName>
					<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
					<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
					<ms:email>smith@example.com</ms:email>
				</ms:Person>
			</ms:contact>
			<!-- mutliple element; please repeat for different keywords-->
			<ms:keyword xml:lang="en">ML model</ms:keyword>
			<ms:keyword xml:lang="en">Machine Learning</ms:keyword>
			<!-- RECOMMENDED; mutliple element; please repeat for different domains -->
			<!-- details for the organization that adds this resource in ELG -->
			<ms:resourceProvider>
				<ms:Organization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">Full name of the organization</ms:organizationName>
					<ms:website>http://www.example.com</ms:website>
				</ms:Organization>
			</ms:resourceProvider>
			<!-- RECOMMENDED element for citation purposes; the date the resource was first made available to the public and not just in ELG -->
			<ms:publicationDate>2020-01-09</ms:publicationDate>
			<!-- RECOMMENDED element; it can be one or more persons (repeat the element) or an organization  -->
			<ms:resourceCreator>
				<ms:Person>
					<ms:actorType>Person</ms:actorType>
					<ms:surname xml:lang="en">Smith</ms:surname>
					<ms:givenName xml:lang="en">John</ms:givenName>
					<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
					<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
					<ms:email>smith@example.com</ms:email>
				</ms:Person>
			</ms:resourceCreator>
			<!-- recommended: you can use one of the values from the LT taxonomy (recommended practice) or a free text value (you can also add the URL link to a class from another ontology) -->
			<ms:intendedApplication>
				<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/InformationExtraction</ms:LTClassRecommended>
			</ms:intendedApplication>
			<!-- a link to a document, e.g. a user manual, a research article describing the resource, etc.-->
			<ms:isDocumentedBy>
				<ms:title xml:lang="en">free text for a document, e.g. title, full bibliographic record</ms:title>
				<!-- recommended: please provide a DOI or URL link to the document; if it's a DOI, please use the format of the example below -->
				<ms:DocumentIdentifier ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/doi">https://doi.org/10.1371/journal.pone.0077056</ms:DocumentIdentifier>
			</ms:isDocumentedBy>
			<ms:LRSubclass>
				<ms:LanguageDescription>
					<ms:lrType>LanguageDescription</ms:lrType>
					<ldSubclass>http://w3id.org/meta-share/meta-share/model</ldSubclass>
					<ms:LanguageDescriptionSubclass>
						<ms:Model>
							<ms:ldSubclassType>Model</ms:ldSubclassType>
							<ms:modelType>
								<ms:modelTypeRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:modelTypeRecommended>
							</ms:modelType>
							<ms:modelFunction>
								<ms:modelFunctionRecommended>http://w3id.org/meta-share/meta-share/unspecified</ms:modelFunctionRecommended>
							</ms:modelFunction>
							<!-- RECOMMENDED -->
							<ms:modelVariant>factored</ms:modelVariant>
							<ms:typesystem>
								<ms:resourceName xml:lang="en">Universal dependencies</ms:resourceName>
								<ms:version>undefined</ms:version>
							</ms:typesystem>
							<ms:method>http://w3id.org/meta-share/omtd-share/DeepLearning</ms:method>
							<ms:developmentFramework><DevelopmentFrameworkRecommended>http://w3id.org/meta-share/meta-share/TensorFlow</DevelopmentFrameworkRecommended></ms:developmentFramework>
							<ms:trainingCorpusDetails xml:lang="en">Trained on a corpus of tweets</ms:trainingCorpusDetails>
						</ms:Model>
					</ms:LanguageDescriptionSubclass>
					<ms:LanguageDescriptionMediaPart>
						<ms:LanguageDescriptionTextPart>
							<ms:ldMediaType>LanguageDescriptionTextPart</ms:ldMediaType>
							<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
							<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
							<ms:language>
								<ms:languageTag>en</ms:languageTag>
								<ms:languageId>en</ms:languageId>
							</ms:language>
							<ms:metalanguage>
								<ms:languageTag>en</ms:languageTag>
								<ms:languageId>en</ms:languageId>
							</ms:metalanguage>
						</ms:LanguageDescriptionTextPart>
					</ms:LanguageDescriptionMediaPart>
					<ms:DatasetDistribution>
						<!-- use to declare whether the resource can be downloaded or is accessible through a user interface, SPARQL endpoint, etc. -->
						<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
						<!-- one of the following is mandatory; if the resource can be downloaded without any intermediate pages or clicks, please use downloadLocation; if accessible from an informative page that contains the download location, please use accessLocation -->
						<ms:downloadLocation>ftp://www.download.com</ms:downloadLocation>
						<ms:accessLocation>http://www.access.com</ms:accessLocation>
						<!-- recommended -->
						<ms:samplesLocation>http://samples.com</ms:samplesLocation>
						<ms:distributionTextFeature>
							<ms:size>
								<ms:amount>630</ms:amount>
								<ms:sizeUnit><ms:sizeUnitRecommended>http://w3id.org/meta-share/meta-share/five-gram</ms:sizeUnitRecommended></ms:sizeUnit>
							</ms:size>
							<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/Xml</ms:dataFormatRecommended></ms:dataFormat>
							<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
						</ms:distributionTextFeature>
						<ms:licenceTerms>
							<ms:licenceTermsName xml:lang="en">Creative Commons By 4.0</ms:licenceTermsName>
							<ms:licenceTermsURL>http://www.cc-by.com</ms:licenceTermsURL>
							<ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
							<ms:LicenceIdentifier ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX">cc-by-4.0</ms:LicenceIdentifier>
						</ms:licenceTerms>
					</ms:DatasetDistribution>
					<ms:personalDataIncluded>http://w3id.org/meta-share/meta-share/noP</ms:personalDataIncluded>
					<ms:sensitiveDataIncluded>http://w3id.org/meta-share/meta-share/noS</ms:sensitiveDataIncluded>
				</ms:LanguageDescription>
			</ms:LRSubclass>
		</ms:LanguageResource>
	</ms:DescribedEntity>
</ms:MetadataRecord>
