<?xml version="1.0" encoding="UTF-8"?>
<!--Pre-filled metadata record as an example for ELG-compliant services
Use case: ELG-compliant Machine Translation service
https://european-language-grid.readthedocs.io/en/latest/all/3_Contributing/Service.html# -->
<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://w3id.org/meta-share/meta-share/" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://live.european-language-grid.eu/metadata-schema/ELG-SHARE.xsd">
	<ms:DescribedEntity>
		<ms:LanguageResource>
			<ms:entityType>LanguageResource</ms:entityType>
			<!-- MANDATORY - please try to use "unique" names for the resource, rather than a general title such as "tagger for English" -->
			<ms:resourceName xml:lang="en">MT service X</ms:resourceName>
			<!-- RECOMMENDED - Short name used for the service; please try to use "unique" names for the resource, rather than a general title such as "tagger for English" -->
			<ms:resourceShortName xml:lang="en">MTX</ms:resourceShortName>
			<!-- RECOMMENDED - Short description of the service for human readers; please, try to provide as much information needed to help the users understand what the service does and how it can be used -->
			<ms:description xml:lang="en">A machine translation (MT) service for German-to-English translation based on the Marian machine translation framework. The translation model is a basic transformer model trained on ca 13.3M sentence pairs using Marian NMT</ms:description>
			<!-- MANDATORY if you have a version number, replace; otherwise, leave as is -->
			<ms:version>1.0.0 (automatically assigned)</ms:version>
			<!-- MANDATORY additionalInfo can be either a general email address or a landing page (a web page with general information, including contact details; if you want, you can also provide a contact person details);  can be repeated  -->
			<ms:additionalInfo>
				<ms:email>info@example.com</ms:email>
				<!--ms:landingPage>http://www.example.com</ms:landingPage> -->
			</ms:additionalInfo>
			<!-- RECOMMENDED element; use it only if you want, but PROVIDE an email for the person in this case -->
			<ms:contact>
				<ms:Person>
					<ms:actorType>Person</ms:actorType>
					<ms:surname xml:lang="en">Smith</ms:surname>
					<ms:givenName xml:lang="en">John</ms:givenName>
					<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
					<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
					<ms:email>smith@example.com</ms:email>
				</ms:Person>
			</ms:contact>
			<!-- MANDATORY; mutliple element; please repeat for different keywords-->
			<ms:keyword xml:lang="en">machine translation</ms:keyword>
			<ms:keyword xml:lang="en">another keyword</ms:keyword>
			<!-- RECOMMENDED; details for the organization that adds this resource in ELG; PROVIDE website for the organization -->
			<ms:resourceProvider>
				<ms:Organization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">Company X</ms:organizationName>
					<ms:website>http://www.organization.com</ms:website>
				</ms:Organization>
			</ms:resourceProvider>
			<!-- RECOMMENDED element for citation purposes; the date the resource was first made available to the public and not just in ELG -->
			<ms:publicationDate>2020-01-09</ms:publicationDate>
			<!-- RECOMMENDED element; it can be one or more persons (repeat the element) or an organization;   for person please provide email, for organizations please provide website  -->
			<ms:resourceCreator>
				<ms:Person>
					<ms:actorType>Person</ms:actorType>
					<ms:surname xml:lang="en">Smith</ms:surname>
					<ms:givenName xml:lang="en">John</ms:givenName>
					<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
					<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
					<ms:email>smith@example.com</ms:email>
				</ms:Person>
			</ms:resourceCreator>
			<!-- MANDATORY element; please add both your project and the ELG project here -->
			<ms:fundingProject>
				<ms:projectName xml:lang="en">Open Call Project X</ms:projectName>
				 <!--  PROVIDE website if possible;  -->
				<ms:website>http://www.project.com</ms:website>
			</ms:fundingProject>
			<ms:fundingProject>
				<ms:projectName xml:lang="en">European Language Grid</ms:projectName>
				<ms:website>https://www.european-language-grid.eu/</ms:website>
			</ms:fundingProject>
			<ms:intendedApplication>
				<!-- MANDATORY for Open Call projects; you can use one of the values from the LT taxonomy (recommended practice) or a free text value (you can also add the URL link to a class from another ontology); please add here the BROADER area where the service can be used ;  can be repeated -->
				<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
			</ms:intendedApplication>
			<ms:LRSubclass>
				<ms:ToolService>
					<ms:lrType>ToolService</ms:lrType>
					<!-- MANDATORY you can use one of the values from the LT taxonomy (recommended practice) or a free text value (you can also add the URL link to a class from another ontology); it's a multiple element and you can repeat if you want to add more specific functions -->
					<ms:function>
						<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
					</ms:function>
					<ms:SoftwareDistribution>
						<!-- MANDATORY - default set for services provided by Open Call projects while "under construction"; please DON'T CHANGE the following values: 
			softwareDistributionImage is a docker image,
dockerDownloadLocation is the text to be used for pulling the docker image, e.g. registry.gitlab.com/european-language-grid/expertsystem/cogito-discover:4.0
			executionLocation the REST endpoint used to invoke the service -->
						<ms:SoftwareDistributionForm>http://w3id.org/meta-share/meta-share/dockerImage</ms:SoftwareDistributionForm>
						<ms:executionLocation>http://localhost:8080/mt/process/</ms:executionLocation>
						<ms:dockerDownloadLocation>registry.gitlab.com/EXAMPLE</ms:dockerDownloadLocation>
						<!-- licenceTerms is a MANDATORY element; if you haven't already decided what licence to use, please use the following values and you can change them at a later stage; if you have your own licence, please use a unique name for each licence at licenceTermsName (e.g. serviceName ToS or organization ToS) and also a url with the text for the licence;  -->
						<ms:licenceTerms>
							<ms:licenceTermsName xml:lang="en">Apache License 2.0</ms:licenceTermsName>
							<ms:licenceTermsURL>http://www.apache.org/licenses/LICENSE-2.0</ms:licenceTermsURL>
							<ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
							<ms:conditionOfUse>http://w3id.org/meta-share/meta-share/shareAlike</ms:conditionOfUse>
						</ms:licenceTerms>
					</ms:SoftwareDistribution>
					<!-- MANDATORY for open call projects; please, specify whether the service is language dependent or not -->
					<ms:languageDependent>true</ms:languageDependent>
					<!-- MANDATORY for open call projects; the following elements provide information on what the service takes as input -->
					<ms:inputContentResource>
						<!-- MANDATORY you can specify whether the service takes a file, corpus or text input by the user -->
						<ms:processingResourceType>http://w3id.org/meta-share/meta-share/file1</ms:processingResourceType>
						<!-- MANDATORY if the service is language dependent; the language(s) that can be processed by the tool/service -->
						<ms:language>
							<!-- the languageTag combines the codes from languageId, scriptId, regionId and variantId accoding to the BCP-47 guidelines (RFC 5646); in the XSD there is no validation as to the validity of the tag, but the system validates the input metadata and the editor produces the correct id on the basis of the subelements (language, script, region and variant); ONLY languageTag and languageId are mandatory; the codes are taken from ISO 639-1 (two-letter codes) and, if not covered, from ISO 639-3; you can find the language codes at https://iso639-3.sil.org/code_tables/639/data ;  can be repeated  -->
							<ms:languageTag>en-US</ms:languageTag>
							<ms:languageId>en</ms:languageId>
							<!-- ms:scriptId></ms:scriptId -->
							<ms:regionId>US</ms:regionId>
							<!--ms:variantId></ms:variantId-->
						</ms:language>
						<!-- RECOMMENDED; for MT services, text is the default media type; if you want you can further specify data formats and character encoding of the input text -->
						<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
						<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormatRecommended></ms:dataFormat>
					</ms:inputContentResource>
					<!-- MANDATORY for MT tools; the only mandatory element is processingResourceType which encodes whether a tool/service produces as output a file, corpus (set of files), etc.; for MT tools, please specify also the target language -->
					<ms:outputResource>
						<ms:processingResourceType>http://w3id.org/meta-share/meta-share/file1</ms:processingResourceType>
						<!-- the languageTag combines the codes from languageId, scriptId, regionId and variantId accoding to the BCP-47 guidelines (RFC 5646); in the XSD there is no validation as to the validity of the tag, but the system validates the input metadata and the editor produces the correct id on the basis of the subelements (language, script, region and variant); ONLY languageTag and languageId are mandatory; the codes are taken from ISO 639-1 (two-letter codes) and, if not covered, from ISO 639-3; you can find the language codes at https://iso639-3.sil.org/code_tables/639/data  -->
						<ms:language>
							<ms:languageTag>el</ms:languageTag>
							<ms:languageId>el</ms:languageId>
							<!-- ms:scriptId></ms:scriptId -->
							<!--ms:regionId></ms:regionId-->
							<!--ms:variantId></ms:variantId-->
						</ms:language>
						<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
						<ms:dataFormat><ms:dataFormatRecommended>http://w3id.org/meta-share/omtd-share/Json</ms:dataFormatRecommended></ms:dataFormat>
						<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
					</ms:outputResource>
					<!-- the following two are RECOMMENDED elements; you can use them to specify the framework and/or programming languages that you have used for the development of the service -->
					<ms:developmentFramework><ms:DevelopmentFrameworkRecommended>http://w3id.org/meta-share/meta-share/TensorFlow</ms:DevelopmentFrameworkRecommended></ms:developmentFramework>
					<ms:implementationLanguage>Java</ms:implementationLanguage>
					<!-- RECOMMENDED element; see https://ec.europa.eu/research/participants/data/ref/h2020/wp/2014_2015/annexes/h2020-wp1415-annex-g-trl_en.pdf for explanations -->
					<ms:trl>http://w3id.org/meta-share/meta-share/trl3</ms:trl>
					<ms:evaluated>false</ms:evaluated>
				</ms:ToolService>
			</ms:LRSubclass>
		</ms:LanguageResource>
	</ms:DescribedEntity>
</ms:MetadataRecord>
