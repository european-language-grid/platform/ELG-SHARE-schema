chown -R lighttpd:lighttpd /flask_app/static
lighttpd -f /etc/lighttpd/lighttpd.conf
cd flask_app/flask_app
gunicorn app:app --timeout=180 --bind=0.0.0.0 \
--error-logfile log/api.log: --log-file log/api.log \
--reload --capture-output --enable-stdio-inheritance