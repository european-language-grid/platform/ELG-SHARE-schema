FROM python:3.8.1-alpine3.11

RUN apk update \
    && apk add --virtual build-deps build-base gcc musl-dev \
    && apk add --no-cache bash git openssh lighttpd openrc \
    && apk add libxml2-dev libxslt-dev

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt
ADD ./flask_app /flask_app
COPY ./Schema /flask_app/static
COPY ./deployment/xs3p /flask_app/static/xsl
COPY ./deployment/lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY ./deployment/deploy.sh /
RUN touch /flask_app/flask_app/log/api.log
RUN chmod +x /deploy.sh
ENTRYPOINT /deploy.sh