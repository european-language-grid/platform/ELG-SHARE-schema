<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:ms="http://w3id.org/meta-share/meta-share/">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="@* | node()" name="identity-copy">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="//ms:Person | //ms:metadataCurator | //ms:metadataCreator">
		<xsl:copy>
			<xsl:copy-of select="ms:actorType"/>
			<xsl:copy-of select="ms:surname"/>
			<xsl:copy-of select="ms:givenName"/>
			<xsl:copy-of select="ms:PersonalIdentifier"/>
			<xsl:element name="ms:email">person@organization.org</xsl:element>
		</xsl:copy>
	</xsl:template>
	
		<xsl:template match="//ms:additionalInfo/ms:email"/>
		
</xsl:stylesheet>
