# ELG Script
This software has been developed under the umbrella of the ELG initiative
(https://www.european-language-grid.eu/).

## About
This script converts the metadata elements from the ELRC-SHARE metadata schema 
(derived from META-SHARE) into ELRA metadata (META-SHARE metadata 3.1). The 
script converts XML files.

## Environment
* Python 2
* Python 3

Used with Python 2.7 and Python 3.6 in development.

## Command

    python process_open_data_resources_elra.py examples/resource examples/identifiers_elra.xlsx
